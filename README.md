# README #

Very simple Tic Tac Toe game for Android. Based on 'The Complete Android Development Course' on Udemy.com (https://www.udemy.com/the-complete-android-developer-course)

### Functions ###

* Play a game 'tic tac toe' with 2 players. After the games end you are able to start a new game
* Version 1.0

### How do I get set up? ###

* Import the source in Android Studio and build the app